using Xamarin.Forms.Xaml;
using Xamarin.Forms;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
[assembly: ExportFont("Raleway.ttf")]
[assembly: ExportFont("Raleway_Italic.ttf")]
[assembly: ExportFont("Raleway_Black.ttf")]
[assembly: ExportFont("Raleway_BlackItalic.ttf")]
[assembly: ExportFont("Raleway_Bold.ttf")]
[assembly: ExportFont("Raleway_BoldItalic.ttf")]
[assembly: ExportFont("Raleway_ExtraBold.ttf")]
[assembly: ExportFont("Raleway_ExtraBoldItalic.ttf")]