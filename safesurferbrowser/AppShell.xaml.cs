﻿using safesurferbrowser.ViewModels;
using safesurferbrowser.Views;
using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace safesurferbrowser
{
    public partial class AppShell : Xamarin.Forms.Shell
    {
        public static string adminPassTemp = "";
        public AppShell()
        {
            InitializeComponent();
            Routing.RegisterRoute(nameof(MainPage), typeof(MainPage));
            Routing.RegisterRoute(nameof(LoginPage), typeof(LoginPage));
            Navigation.PushModalAsync(new LoginPage());            
        }

        

        private void MenuItem_Clicked(object sender, EventArgs e)
        {
            Navigation.PushModalAsync(new LoginPage());
        }
    }
}
