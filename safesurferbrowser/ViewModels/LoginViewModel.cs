﻿using safesurferbrowser.Views;
using safesurferbrowser.Models;
using safesurferbrowser.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using Xamarin.Forms;
using SQLite;

namespace safesurferbrowser.ViewModels
{
    public class LoginViewModel : BaseViewModel
    {
        public Command LoginCommand { get; }
        private IDataStore _datastore;
        public Command CreatePass { get; }       
        
         
        public LoginViewModel()
        {
            
            LoginCommand = new Command(OnLoginClicked);
            CreatePass = new Command(OnCreatePass); 
            if(AppShell.adminPassTemp == "")
            {
                LoginVis = false;
                AdminCreateVis = true;
            }

            else
            {
                LoginVis = true;
                AdminCreateVis = false;
            }


        }

        


        private async void OnLoginClicked(object obj)
        {
            // Prefixing with `//` switches to a different navigation stack instead of pushing to the active one
            if (LoginPass == admin.password)
            {                
                await Shell.Current.GoToAsync("//MainPage");
            }
        }

        private ObservableCollection<Person> users = new ObservableCollection<Person>();
        
        public ObservableCollection<Person> Users
        {
            get { return users; }
            set
            {
                users = value;
                OnPropertyChanged("UserListView");
            }
        }


        private async void OnCreatePass(object obj)
        {
            admin.password = AdminPass;
            AppShell.adminPassTemp = AdminPass;
            LoginVis = true;
            AdminCreateVis = false;            
            Users.Add(new Person("Admin", 0, admin.password, "", 0 ));            
        }

        

        private bool _adminCreateVis;
        public bool AdminCreateVis
        {
            get { return _adminCreateVis; }
            set
            {
                if(_adminCreateVis != value)
                {
                    _adminCreateVis = value;
                    OnPropertyChanged("AdminCreateVis");
                }
            }
        }

        private bool _loginVis;
        public bool LoginVis
        {
            get { return _loginVis; }
            set 
            { 
                if(_loginVis != value)
                {
                    _loginVis = value;
                    OnPropertyChanged("LoginVis");
                }
            }
        }

        private string _loginPass;
        public string LoginPass
        {
            get { return _loginPass; }
            set 
            { 
            if(_loginPass != value)
                {
                    _loginPass = value;
                    OnPropertyChanged("LoginPass");
                }
            }
        }

        private string _adminPass;
        public string AdminPass
        {
            get { return _adminPass; }
            set
            {
                if(_adminPass != value)
                {
                    _adminPass = value;
                    OnPropertyChanged("NewAdminPass");
                }
            }
        }

        private string _password;
        public string Password
        {
            get { return _password; }
            set
            {
                if(_password != value)
                {
                    _password = value;                    
                }                
            }
        }

        private string _email;
        public string Email
        {
            get { return _email; }
            set
            {
                if (_email != value)
                {
                    _email = value;
                }
            }
        }

        private string _home;
        public string Home
        {
            get { return _email; }
            set
            {
                if (_email != value)
                {
                    _email = value;
                }
            }
        }
    }
}
