﻿using System;
using System.Collections.Generic;
using System.Text;

namespace safesurferbrowser.Models
{
    public class AdminSettingsData
    {        
        public string password { get; set; }
        public string email { get; set; }
        public string home { get; set; }        
    }

    public class Users
    {
        public string userName { get; set; }


        public Users(string _userName)
        {
            userName = _userName;

        }
    }

    public class Person : Users
    {
        public int userAge { get; set; }
        public string userPassword { get; set; }
        public string userPic { get; set; }
        public int userEnabled { get; set; }

        public Person(string _userName, int _userAge, string _userPassword, string _userPic, int _state) : base(_userName)
        {
            userAge = _userAge;
            userPassword = _userPassword;
            userPic = _userPic;
            userEnabled = _state;

        }
    }

    public class FavoritesData : Users
    {
        public string siteName { get; set; }
        public string siteUrl { get; set; }
        public string imageRef { get; set; }

        public FavoritesData(string _SiteName, string _SiteUrl, string _ImageRef, string _userName) : base(_userName)
        {
            userName = _userName;
            siteName = _SiteName;
            siteUrl = _SiteUrl;
            imageRef = _ImageRef;
        }
    }

    public class WebHistory : Users
    {
        public string siteUrlHis { get; set; }
        public DateTime date { get; set; }
        public int flagged { get; set; }
        public WebHistory(string _SiteName, DateTime _date, string _userName, int _flagged) : base(_userName)
        {
            userName = _userName;
            date = _date;
            siteUrlHis = _SiteName;
            flagged = _flagged;
        }
    }

    public class GlobalBlockList
    {
        public string urlBlockItem { get; set; }
        public GlobalBlockList(string _urlBlockItem)
        {
            urlBlockItem = _urlBlockItem;
        }
    }

    public class UserBlocklist : Users
    {
        public string urlBlockItem { get; set; }
        public UserBlocklist(string _UrlBlockItem, string _userName) : base(_userName)
        {
            urlBlockItem = _UrlBlockItem;
        }
    }

    public class GlobalAllowList
    {
        public string urlAllowItem { get; set; }
        public GlobalAllowList(string _urlAllowItem)
        {
            urlAllowItem = _urlAllowItem;
        }
    }

    public class UserAllowlist : Users
    {
        public string urlAllowItem { get; set; }
        public UserAllowlist(string _UrlAllowItem, string _userName) : base(_userName)
        {
            urlAllowItem = _UrlAllowItem;
        }
    }

    public class ProfilePics
    {
        public Uri profPic { get; set; }
        public ProfilePics(Uri _profPic)
        {
            profPic = _profPic;
        }
    }
}
